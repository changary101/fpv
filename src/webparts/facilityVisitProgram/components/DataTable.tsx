import * as React from 'react';
import {ListView, IViewField, SelectionMode, GroupOrder, IGrouping } from '@pnp/spfx-controls-react/Lib/ListView';
import {IListItem, SpfxService} from './spfxService';
import {WebPartContext} from '@microsoft/sp-webpart-base';
import {PageContext} from '@microsoft/sp-page-context';


interface IDataTableState{
    ListData:IListItem[];
}

interface IProps{
 context:WebPartContext;
}
const groupByFields:IGrouping[] = [{
    name:'Region',
    order:GroupOrder.descending
  }];

const viewFields:IViewField[] = [
    {
      name:'Title',
      displayName:'Title',
      isResizable:true,
      sorting:true,
      minWidth:0,
      maxWidth:150
    },
    {
      name:'Region',
      displayName:'Region',
      linkPropertyName:'c',
      isResizable:true,
      sorting:true,
      minWidth:0,
      maxWidth:100
    }
  ];

export default class DataTable extends React.Component<IProps, IDataTableState>{
    private _SPfxService:SpfxService;
    constructor(props:IProps, state:IDataTableState){
        super(props);
        this.state = {
            ListData:[],
        };
        this._SPfxService = new SpfxService(props.context, props.context.pageContext);
    }
    private _getSelection(items:any[]):void{
        console.log('selected items:', items);
    }
    private _updateList(){
        this._SPfxService.getAllrecords("ListView").then((result:IListItem[]) =>{
          this.setState({ListData:result});
        });
      }
    public componentDidMount(){
        this._updateList();
    }

    public render(){
        return (
            <ListView
            items={this.state.ListData}
            showFilter={true}
            filterPlaceHolder="Search..."
            compact={true}
            selectionMode={SelectionMode.multiple}
            selection={this._getSelection}
            groupByFields = {groupByFields}
            viewFields = {viewFields}
            
            ></ListView>
            
        );
    }
}
