import * as React from 'react';
import {useState} from 'react';
import styles from './FacilityVisitProgram.module.scss';
import { IFacilityVisitProgramProps } from './IFacilityVisitProgramProps';

import Main from './main';



export default class FacilityVisitProgram extends React.Component<IFacilityVisitProgramProps> {
  constructor(props:IFacilityVisitProgramProps){
    super(props);
  }
  public componentDidMount(){

  }
  public render(): React.ReactElement<IFacilityVisitProgramProps> {

    return (

      <Main context={this.props.context}/>


    );
  }
}
